<?php

//"http://localhost/ApiServer/Api.php"

//$_GET[];
$method = $_SERVER['REQUEST_METHOD']; 
$service_url = 'http://localhost/ApiServer/Api.php/';
$service_url .= str_replace(["=","&"], "/", substr($_SERVER['REQUEST_URI'], 24));
//var_dump($service_url);die();

// Create Curl Resource
$curl = curl_init();

// Set URL
curl_setopt($curl, CURLOPT_URL, $service_url);

// Return the transfer as a string
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

// Execute and return the response
$curl_response = curl_exec($curl);

//var_dump($curl_response);die();
if (!$curl_response){
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('Error ocured during curl execution. More info: ' . var_export($info));
}

// Close curl response
curl_close($curl);

$response = json_decode($curl_response);

// Dump the response
//var_dump($response);die();